package com.j2core.sts.stssecurehashfile.stsbuffer;

import java.util.Queue;

/**
 * Created by sts on 11/10/15.
 */

/**
 * Object for save paragraphs and them amount
 */
public class ParagraphBuffer<T> {

    private volatile boolean flagEndFile = false;   // flag end file
    public Queue<T> dataQueue;                      // queue for save paragraphs

    /**
     * Constructor for object STSBuffer
     *
     * @param dataQueue   queue for save paragraphs
     */
    public ParagraphBuffer(Queue<T> dataQueue) {
        this.dataQueue = dataQueue;
    }

    public boolean isFlagEndFile() {
        return flagEndFile;
    }

    public void setFlagEndFile(boolean flagEndFile) {
        this.flagEndFile = flagEndFile;
    }
}

package com.j2core.sts.stssecurehashfile.stsbuffer;

import java.util.Queue;

/**
 * Created by sts on 11/19/15.
 */

/**
 * The class is buffer for save paragraphsText and his information
 */
public class ParagraphsDataBuffer<T> extends ParagraphBuffer<T> {

    private int amountWordsInParagraph;               // amount words in the paragraphsText
    private int minLengthWord = 0;                    // min word's length
    private int maxLengthWord = 1;                    // max word's length
    private int amountSymbolPunctuation;              // amount symbols punctuation
    private int averageLengthWord;                    // average word's length
    private int lengthAllWords;                       // all words length
    private String paragraphsText = "";               // paragraphsText
    private String sha1paragraph = "";                // value paragraphsText's SHA1


    /**
     * Constructor for object STSWriterBuffer
     *
     * @param dataQueue  // queue for save paragraphsText's words
     */
    public ParagraphsDataBuffer(Queue<T> dataQueue) {
        super(dataQueue);
    }

    public String getSha1paragraph() {
        return sha1paragraph;
    }

    public void setSha1paragraph(String sha1paragraph) {
        this.sha1paragraph = sha1paragraph;
    }

    public String getParagraphsText() {
        return paragraphsText;
    }

    public void setParagraphsText(String paragraphsText) {
        this.paragraphsText = paragraphsText;
    }

    public int getAmountWordsInParagraph() {
        return amountWordsInParagraph;
    }

    public void setAmountWordsInParagraph(int amountWordsInParagraph) {
        this.amountWordsInParagraph = amountWordsInParagraph;
    }

    public int getMinLengthWord() {
        return minLengthWord;
    }

    public void setMinLengthWord(int minLengthWord) {
        this.minLengthWord = minLengthWord;
    }

    public int getMaxLengthWord() {
        return maxLengthWord;
    }

    public void setMaxLengthWord(int maxLengthWord) {
        this.maxLengthWord = maxLengthWord;
    }

    public int getAmountSymbolPunctuation() {
        return amountSymbolPunctuation;
    }

    public void setAmountSymbolPunctuation(int amountSymbolPunctuation) {
        this.amountSymbolPunctuation = amountSymbolPunctuation;
    }

    public int getAverageLengthWord() {
        return averageLengthWord;
    }

    public void setAverageLengthWord(int averageLengthWord) {
        this.averageLengthWord = averageLengthWord;
    }

    public int getLengthAllWords() {
        return lengthAllWords;
    }

    public void setLengthAllWords(int lengthAllWord) {
        this.lengthAllWords = lengthAllWord;
    }

    @Override
    public String toString() {
        return " amount words = " + amountWordsInParagraph +
                ", amount symbol punctuation = " + amountSymbolPunctuation +
                ", average length word = " + averageLengthWord +
                ", max length word = " + maxLengthWord +
                ", min length word = " + minLengthWord;
    }
}

package com.j2core.sts.stssecurehashfile;

import com.j2core.sts.stssecurehashfile.stsbuffer.Paragraph;
import com.j2core.sts.stssecurehashfile.stsbuffer.ParagraphBuffer;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * The class read paragraphs from file and save in the STSBuffer
 */
public class ParagraphsReader implements Runnable{

    public static final Logger logger = Logger.getLogger(ParagraphsReader.class);
    private File originalFile;                          // file for read
    ParagraphBuffer<Paragraph> buffer;                  // buffer for save paragraphs
    final Object syncReader;                            // object for synchronization
    static int paragraphsCounter = 1;                   //  amount read paragraphs

    /**
     * Constructor for thread STSFileReader
     *
     * @param originalFile        file for read paragraphs
     * @param sync                object for synchronization
     * @param buffer              buffer for save paragraphs and his amount
     */
    public ParagraphsReader(File originalFile, ParagraphBuffer<Paragraph> buffer, Object sync) {
        this.originalFile = originalFile;
        this.syncReader = sync;
        this.buffer = buffer;
    }


    @Override
    public void run() {

        logger.info("************************* Started thread reader. ");

        try {
            Scanner scanner = new Scanner(originalFile);
            boolean indicator = true;
            StringBuilder entry = new StringBuilder();

            while (scanner.hasNextLine()) {

                if (buffer.dataQueue.size() < Coordinator.amountDataForOperation) {

                    // read paragraph
                    while (indicator) {
                        entry.append(scanner.nextLine());
                        if (entry.indexOf("/r/n") < 0) {
                            indicator = false;
                        }
                    }

                    if (entry.length() > 0) {

                        buffer.dataQueue.add(new Paragraph(paragraphsCounter++, entry.toString()));
                        logger.info(" Save paragraph in the buffer. Read " + paragraphsCounter + "paragraphs ");
                        entry.delete(0, entry.length());
                    }
                    indicator = true;

                    // notify Worker thread
                    synchronized (syncReader) {
                        syncReader.notify();
                    }
                }else {

                    synchronized (syncReader){
                        syncReader.wait();
                    }

                }
            }

            buffer.setFlagEndFile(true);
            logger.info(" Change parameter flagEndFile. File read all.");

        } catch (FileNotFoundException e) {
            logger.error(" Error with read file " + e);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // notify all Worker threads
        synchronized (syncReader) {
            syncReader.notifyAll();
        }

        logger.info(" ********************Thread Reader ended work.");

    }
}

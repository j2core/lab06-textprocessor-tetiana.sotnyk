package com.j2core.sts.stssecurehashfile;

import com.j2core.sts.stssecurehashfile.stsbuffer.Paragraph;
import com.j2core.sts.stssecurehashfile.stsbuffer.ParagraphsDataBuffer;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by sts on 11/18/15.
 */
/*
  The class write paragraph's data and file data in the new file
*/
public class ParagraphsWriter  implements Runnable{

    public static final Logger logger = Logger.getLogger(ParagraphsWriter.class);
    final Object syncWriter;                                    // object for synchronization
    File newFile;                                               // new file for write data
    ParagraphsDataBuffer<Paragraph> buffer;                     // buffer for save paragraph's data
    String nameNewFile;                                         // name new file
    static int paragraphCounter = 1;                            // counter for Paragraphs
    String informationForWrite = "";
    List<Paragraph> writerBuffer = new LinkedList<>();
    String isEmptyString = "";
    

    /**
     * Constructor for thread STSFileWriter
     *
     * @param syncWriter       object for synchronization
     * @param nameNewFile      name new file
     * @param buffer           buffer with paragraph's data
     */
    public ParagraphsWriter(Object syncWriter, String nameNewFile, ParagraphsDataBuffer<Paragraph> buffer){

        this.syncWriter = syncWriter;
        this.buffer = buffer;
        this.nameNewFile = nameNewFile;

    }

    @Override
    public void run() {

        logger.info("********************* Started thread STSFileWriter");

        // create new file
        newFile = new File(nameNewFile);

        // create new fileWriter
        try (java.io.FileWriter writer = new java.io.FileWriter(newFile)){

                while (!buffer.dataQueue.isEmpty() || !buffer.isFlagEndFile()) {

                    if (buffer.dataQueue.isEmpty()){

                        synchronized (syncWriter){
                            try {
                                syncWriter.wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }

                    }else {
                        int counter = 0;
                        while (!buffer.dataQueue.isEmpty() && counter < 5){

                            writerBuffer.add(buffer.dataQueue.remove());
                            counter++;
                        }

                        if (!writerBuffer.isEmpty()){

                            writerBuffer = sortedInformationForWrite(writerBuffer);

                            informationForWrite = getInformationForWrite(writerBuffer);
                            if (!informationForWrite.equals(isEmptyString)){

                                writer.write(informationForWrite);
                                logger.info(" Wrote paragraph and his data. Amount wrote paragraphs  = " + (paragraphCounter - 1));
                                informationForWrite = isEmptyString;
                            }

                        }

                    }
                }

                logger.info(" Wrote data about all paragraphs.");
                writer.write(System.lineSeparator() + " File has: " + System.lineSeparator() + (paragraphCounter - 1) + " paragraphs, " + buffer.toString());

            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        logger.info(" ********************Thread Writer ended work.");

    }


    /**
     * The method is taking paragraphs and their information from writerBuffer, and writing its in the string
     *
     * @param writerBuffer  collection with Paragraph's objects
     * @return              paragraphs with their information
     */
    protected String getInformationForWrite(List<Paragraph> writerBuffer){

        StringBuilder stringBuilder = new StringBuilder();
        Iterator<Paragraph> paragraphIterator = writerBuffer.iterator();
        while (paragraphIterator.hasNext()){
            Paragraph paragraph = paragraphIterator.next();
            if (paragraph.getNumberParagraph() == paragraphCounter){
                stringBuilder.append(paragraph.getParagraph());
                paragraphIterator.remove();
                paragraphCounter++;
            }else break;
        }

        return stringBuilder.toString();
    }


    /**
     * The method is sorting objects in the writerBuffer
     *
     * @param writerBuffer  collection Paragraph's objects
     * @return              sorted collection Paragraph's objects
     */
    protected List<Paragraph> sortedInformationForWrite(List<Paragraph> writerBuffer){

        Paragraph[] arrayParagraphs = writerBuffer.toArray(new Paragraph[writerBuffer.size()]);

        for (int i = 0; i < arrayParagraphs.length; i++){
            Paragraph minValueCounter = arrayParagraphs[i];
            int index = i;
            for (int j = i+1; j < arrayParagraphs.length; j++){
                if (minValueCounter.compareTo(arrayParagraphs[j]) > 0){
                    minValueCounter = arrayParagraphs[j];
                    index = j;
                }
            }
            if (index != i){
                System.arraycopy(arrayParagraphs, i, arrayParagraphs, i+1, index-i);
                arrayParagraphs[i] = minValueCounter;
            }
        }
        writerBuffer.clear();
        Collections.addAll(writerBuffer, arrayParagraphs);

        return writerBuffer;
    }
}

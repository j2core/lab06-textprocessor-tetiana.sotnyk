package com.j2core.sts.stssecurehashfile;

import com.j2core.sts.stssecurehashfile.stsbuffer.Paragraph;
import com.j2core.sts.stssecurehashfile.stsbuffer.ParagraphsDataBuffer;
import org.testng.Assert;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by sts on 11/27/15.
 */
public class ParagraphsWriterTest {


    @BeforeGroups(groups = "fileWriter")
    public ParagraphsWriter createdParagraphWriter() {

        return new ParagraphsWriter(new Object(), "NewFile", new ParagraphsDataBuffer<Paragraph>(new LinkedList<Paragraph>()));
    }


        @DataProvider(name = "writer")
    public Object[][] dataProviderListParagraphs(){
        return new Object[][]{
                {new Paragraph(1, "1"), new Paragraph(5, "5"), new Paragraph(3,"3"), new Paragraph(2,"2"), new Paragraph(4, "4"), new Paragraph(6, "6")}
        };
    }

    @Test(dataProvider = "writer", groups = "fileWriter")
    public void testSortedInformationForWrite(Paragraph par1, Paragraph par5, Paragraph par3, Paragraph par2, Paragraph par4, Paragraph par6){

        ParagraphsWriter writer = createdParagraphWriter();

        List<Paragraph> paragraphList = new LinkedList<Paragraph>();
        paragraphList.add(par1);
        paragraphList.add(par5);
        paragraphList.add(par3);
        paragraphList.add(par2);
        paragraphList.add(par4);

        paragraphList = writer.sortedInformationForWrite(paragraphList);

        Assert.assertTrue(paragraphList.get(0).getNumberParagraph()==1 && paragraphList.get(1).getNumberParagraph()==2 && paragraphList.get(2).getNumberParagraph()==3 && paragraphList.get(3).getNumberParagraph()==4 && paragraphList.get(4).getNumberParagraph()==5 );
    }


    @Test(groups = "fileWriter", dataProvider = "writer")
    public void testGetInformationForWrite(Paragraph par1, Paragraph par5, Paragraph par3, Paragraph par2, Paragraph par4, Paragraph par6) {

        ParagraphsWriter writer = createdParagraphWriter();

        List<Paragraph> paragraphList = new LinkedList<Paragraph>();
        paragraphList.add(par1);
        paragraphList.add(par2);
        paragraphList.add(par3);
        paragraphList.add(par4);
        paragraphList.add(par6);
        paragraphList.add(par5);

        String result = writer.getInformationForWrite(paragraphList);

        Assert.assertEquals(result, "1234");
    }
}

package com.j2core.sts.stssecurehashfile;


import com.j2core.sts.stssecurehashfile.stsbuffer.Paragraph;
import com.j2core.sts.stssecurehashfile.stsbuffer.ParagraphBuffer;
import org.testng.Assert;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

import java.io.File;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by sts on 11/12/15.
 */
public class ParagraphsReaderTest {

    @BeforeGroups(groups = "fileReader")
    public ParagraphsReader createdParagraphReader(){

        ClassLoader classLoader = this.getClass().getClassLoader();

        Coordinator.amountDataForOperation = 500;

        return new ParagraphsReader(new File(classLoader.getResource("Scott_Fitzgerald.txt").getFile()), new ParagraphBuffer<>(new LinkedBlockingQueue<Paragraph>()), new Object());

    }


    @Test(groups = "fileReader")
    public void testParagraphReaderSize(){

        ParagraphsReader reader = createdParagraphReader();

        reader.run();

        reader.buffer.dataQueue.size();

        Assert.assertTrue(reader.buffer.dataQueue.size() == 262);

    }


    @Test(groups = "fileReader")
    public void testParagraphReaderChangeParameters(){

        ParagraphsReader reader = createdParagraphReader();

        reader.run();

        Assert.assertTrue(reader.buffer.isFlagEndFile());

    }
}

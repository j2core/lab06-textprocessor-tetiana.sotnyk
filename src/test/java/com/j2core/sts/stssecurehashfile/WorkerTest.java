package com.j2core.sts.stssecurehashfile;

import com.j2core.sts.stssecurehashfile.stsbuffer.Paragraph;
import com.j2core.sts.stssecurehashfile.stsbuffer.ParagraphBuffer;
import com.j2core.sts.stssecurehashfile.stsbuffer.ParagraphsDataBuffer;
import org.testng.Assert;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by sts on 11/24/15.
 */
public class WorkerTest {


    @BeforeGroups(groups = "worker")
    public Worker createdTestWorker(){

        ParagraphBuffer<Paragraph> paragraphBuffer = new ParagraphBuffer<>(new LinkedBlockingQueue<Paragraph>(12));
        ParagraphsDataBuffer<Paragraph> paragraphsDataBuffer = new ParagraphsDataBuffer<>(new LinkedBlockingQueue<Paragraph>());
        Object syncReader = new Object();
        Object syncWriter = new Object();

        return new Worker(paragraphBuffer, paragraphsDataBuffer, syncReader, syncWriter);

    }


    @DataProvider(name = "dataParagraph")
    public Object[][] dataProviderDataAboutParagraph() {
        return new Object[][]{
                {6, 9, 52, 2, 13,  "\"About six hours,\" answered Mr. Button, without due consideration."},
                {8, 20, 76, 1, 8, "Mr. Button's son's voice followed him down into the: hall: \"And a cane, father. I want to have a cane.\""},
                {10, 18, 65, 1, 9, "\"Keep it on! Keep it on!\" said Mr. Button hurriedly. He turned to the nurse. \"What'll I do?\""},
                {8, 18, 72, 1, 12, "He went to his father. \"I am grown,\" he announced determinedly. \"I want to put on long trousers.\"", },
                {4, 6, 20, 2, 5, "\"How old is your child, sir?\""},
                {3, 2, 9, 4, 5, "\"Right here.\""}
        };
    }


    @Test(groups = "worker", dataProvider = "dataParagraph")
    public void testCleanParametersInBuffer(int amountSymbolPunctuation, int amountAllWord, int lengthAllWords, int minLength, int maxLength, String paragraph){

        ParagraphsDataBuffer buffer = new ParagraphsDataBuffer<>(new LinkedList<>());
        ParagraphsDataBuffer<String> testBuffer = new ParagraphsDataBuffer<>(new LinkedList<String>());

        testBuffer.setParagraphsText(paragraph);
        testBuffer.setAmountSymbolPunctuation(amountSymbolPunctuation);
        testBuffer.setAmountWordsInParagraph(amountAllWord);
        testBuffer.setLengthAllWords(lengthAllWords);
        testBuffer.setMinLengthWord(minLength);
        testBuffer.setMaxLengthWord(maxLength);
        testBuffer.setAverageLengthWord(lengthAllWords/amountAllWord);

        createdTestWorker().cleanParametersInWorkerBuffer(testBuffer);

        Assert.assertTrue(equalsSTSWriterBuffer(buffer, testBuffer));

    }


    @Test(groups = "worker", dataProvider = "dataParagraph")
    public void testCalculationSymbolsPunctuation(int amountSymbolPunctuation, int amountAllWord, int lengthAllWords, int minLength, int maxLength, String paragraph){

        Assert.assertTrue(amountSymbolPunctuation == createdTestWorker().calculationSymbolsPunctuation(paragraph));

    }


    @Test(groups = "worker", dataProvider = "dataParagraph")
    public void testParserWordsFirst(int amountSymbolPunctuation, int amountAllWord, int lengthAllWords, int minLength, int maxLength, String paragraph){

        Assert.assertTrue(amountAllWord == createdTestWorker().parserWords(paragraph).size());

    }



    @Test(groups = "worker", dataProvider = "dataParagraph")
    public void testParserWordsSecond(int amountSymbolPunctuation, int amountAllWord, int lengthAllWords, int minLength, int maxLength, String paragraph){

        Queue<String> testBuffer = new LinkedList<>();
        char[] data = paragraph.toCharArray();
        int index = 0;
        char space = ' ';
        String allSymbolsPunctuation = ".,:;!?()[]{}<>/|@#$%^&*-+=_~`\"" + space;
        while (index < paragraph.length()) {

            int i = index;
            if (data[index] == space) {
                while (index < paragraph.length() && data[index] == space) {
                    index++;
                    i = index;
                }
            }
            if (index < paragraph.length() && allSymbolsPunctuation.contains(String.valueOf(data[index]))){
                index++;
                i = index;
            }
            while (index < paragraph.length() && (!(allSymbolsPunctuation.contains(String.valueOf(data[index]))))) {
                index++;
            }
            if (index != i) {
                if (index == paragraph.length()) {
                    testBuffer.add(paragraph.substring(i));
                } else {
                    testBuffer.add(paragraph.substring(i, index));
                }
            }
        }

        Worker worker = createdTestWorker();

        Assert.assertTrue(testBuffer.equals(worker.parserWords(paragraph)));

    }


    @Test(groups = "worker", dataProvider = "dataParagraph")
    public void testCalculationLengthAllWordsInBuffer(int amountSymbolPunctuation, int amountAllWord, int lengthAllWords, int minLength, int maxLength, String paragraph){

        Worker worker = createdTestWorker();
        Queue<String> wordsList = worker.parserWords(paragraph);

        Assert.assertTrue(lengthAllWords == worker.calculationLengthAllWordInParagraph(wordsList));

    }


    @Test(groups = "worker", dataProvider = "dataParagraph")
    public void testSearchMinLengthWord(int amountSymbolPunctuation, int amountAllWord, int lengthAllWords, int minLength, int maxLength, String paragraph){

        Worker worker = createdTestWorker();
        Queue<String> wordsList = worker.parserWords(paragraph);

        Assert.assertTrue(minLength == worker.searchMinLengthWord(wordsList));

    }


    @Test(groups = "worker", dataProvider = "dataParagraph")
    public void testSearchMaxLengthWord(int amountSymbolPunctuation, int amountAllWord, int lengthAllWords, int minLength, int maxLength, String paragraph){

        Worker worker = createdTestWorker();
        Queue<String> wordsList = worker.parserWords(paragraph);

        Assert.assertTrue(maxLength == worker.searchMaxLengthWord(wordsList));

    }


    @Test(groups = "worker")
    public void testLastActionsWorkerThreadsTrue(){

        Coordinator.amountThreadsWorker = 1;
        Worker worker = createdTestWorker();

        worker.lastActionsWorkerThreads(worker.writerBuffer);

        Assert.assertTrue(worker.writerBuffer.isFlagEndFile());

    }


    @Test(groups = "worker")
    public void testLastActionsWorkerThreadsFalse(){

        Coordinator.amountThreadsWorker = 5;
        Worker worker = createdTestWorker();

        worker.lastActionsWorkerThreads(worker.writerBuffer);

        Assert.assertFalse(worker.writerBuffer.isFlagEndFile());

    }


    @Test(groups = "worker", dataProvider = "dataParagraph")
    public void testAddedParagraphsInformationInWriterBuffer(int amountSymbolPunctuation, int amountAllWord, int lengthAllWords, int minLength, int maxLength, String text){

        Worker worker = createdTestWorker();
        Paragraph paragraph = new Paragraph(1, text);

        worker.workerBuffer.setAmountWordsInParagraph(amountAllWord);
        worker.workerBuffer.setMinLengthWord(minLength);
        worker.workerBuffer.setMaxLengthWord(maxLength);
        worker.workerBuffer.setAmountSymbolPunctuation(amountSymbolPunctuation);
        worker.workerBuffer.setLengthAllWords(lengthAllWords);
        worker.workerBuffer.setParagraphsText(text);
        worker.paragraph = paragraph;

        worker.addedParagraphsInformationInWriterBuffer();

        Assert.assertTrue(worker.writerBuffer.getAmountWordsInParagraph() == worker.workerBuffer.getAmountWordsInParagraph() &&
                          worker.writerBuffer.getMinLengthWord() == worker.workerBuffer.getMinLengthWord() &&
                          worker.writerBuffer.getMaxLengthWord() == worker.workerBuffer.getMaxLengthWord() &&
                          worker.writerBuffer.getAmountSymbolPunctuation() == worker.workerBuffer.getAmountSymbolPunctuation() &&
                          worker.writerBuffer.getLengthAllWords() == worker.workerBuffer.getLengthAllWords() &&
                          !worker.writerBuffer.dataQueue.isEmpty());

    }


    @Test(groups = "worker", dataProvider = "dataParagraph")
    public void testRunAddedResultInWriterBuffer(int amountSymbolPunctuation, int amountAllWord, int lengthAllWords, int minLength, int maxLength, String text){

        Worker worker = createdTestWorker();
        Paragraph paragraph = new Paragraph(1, text);
        worker.readerBuffer.dataQueue.add(paragraph);
        worker.readerBuffer.setFlagEndFile(true);

        worker.run();

        Assert.assertTrue(!worker.writerBuffer.dataQueue.isEmpty() && worker.readerBuffer.dataQueue.isEmpty());

    }


    public boolean equalsSTSWriterBuffer(ParagraphsDataBuffer buffer, ParagraphsDataBuffer testBuffer){

        if (!buffer.getParagraphsText().equals(testBuffer.getParagraphsText())) return false;
        if (!buffer.getSha1paragraph().equals(testBuffer.getSha1paragraph())) return false;
        if (buffer.getAmountSymbolPunctuation() != testBuffer.getAmountSymbolPunctuation()) return false;
        if (buffer.getAmountWordsInParagraph() != testBuffer.getAmountWordsInParagraph()) return false;
        if (buffer.getLengthAllWords() != testBuffer.getLengthAllWords()) return false;
        if (buffer.getMinLengthWord() != testBuffer.getMinLengthWord()) return false;
        if (buffer.getMaxLengthWord() != testBuffer.getMaxLengthWord()) return false;

        return buffer.getAverageLengthWord() == testBuffer.getAverageLengthWord();
    }
}

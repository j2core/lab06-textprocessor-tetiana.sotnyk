package com.j2core.sts.stssecurehashfile;

import org.testng.annotations.Test;

import java.io.File;

/**
 * Created by sts on 11/27/15.
 */
public class CoordinatorTest {

    @Test
    public void testRun(){

        String nameFile = "Scott_Fitzgerald.txt";
        String nameNewFile = "Data_File_Scott_Fitzgerald.txt";
        int amountThreadsWorker = 8;
        ClassLoader classLoader = this.getClass().getClassLoader();

        File file = new File(classLoader.getResource(nameFile).getFile());

        new Thread(new Coordinator(file, nameNewFile, amountThreadsWorker, 12)).run();

    }
}
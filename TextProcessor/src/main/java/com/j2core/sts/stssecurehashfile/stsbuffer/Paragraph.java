package com.j2core.sts.stssecurehashfile.stsbuffer;

/**
 * Created by sts on 12/15/15.
 */

/**
 * Class for save paragraph's information
 */
public class Paragraph implements Comparable<Paragraph>{

    protected final int numberParagraph;
    protected String paragraph;


    /**
     * Constructor
     *
     * @param numberParagraph   paragraph's number in file
     * @param paragraph         paragraph's text
     */
    public Paragraph(int numberParagraph, String paragraph) {
        this.numberParagraph = numberParagraph;
        this.paragraph = paragraph;
    }

    public int getNumberParagraph() {
        return numberParagraph;
    }

    public String getParagraph() {
        return paragraph;
    }

    public void setParagraph(String paragraph) {
        this.paragraph = paragraph;
    }

    @Override
    public int compareTo(Paragraph obj) {

        return Integer.compare(numberParagraph, obj.getNumberParagraph());
    }

}

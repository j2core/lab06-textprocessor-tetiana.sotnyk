package com.j2core.sts.stssecurehashfile;

import com.j2core.sts.stssecurehashfile.stsbuffer.Paragraph;
import com.j2core.sts.stssecurehashfile.stsbuffer.ParagraphBuffer;
import com.j2core.sts.stssecurehashfile.stsbuffer.ParagraphsDataBuffer;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by sts on 11/19/15.
 */


public class Coordinator implements Runnable{

    public static final Logger logger = Logger.getLogger(Coordinator.class);
    private final Object syncReader = new Object();    // object for synchronization
    private final Object syncWriter = new Object();    // object for synchronization
    static int amountThreadsWorker;                 // amount threads worker
    static int amountDataForOperation;           // max amount paragraphs in the buffer in one time
    protected File oldFile;                      // path file for load
    protected String nameNewFile;                      // name file for saved result file's load


    /**
     * Constructor for object Coordinator
     *
     * @param oldFile           // path file for load
     * @param nameNewFile           // name file for saved result file's load
     * @param amountThreadsWorker   // amount threads worker
     */
    public Coordinator(File oldFile, String nameNewFile, int amountThreadsWorker, int amountDataForOperation) {
        this.oldFile = oldFile;
        this.nameNewFile = nameNewFile;
        Coordinator.amountDataForOperation = amountDataForOperation;
        Coordinator.amountThreadsWorker = amountThreadsWorker;
    }

    @Override
    public void run() {

        logger.info(" Start thread Coordinator");

        // Created reader's and writer's buffers
        ParagraphBuffer<Paragraph> readerBuffer = new ParagraphBuffer<>(new LinkedBlockingQueue<Paragraph>(amountDataForOperation));
        ParagraphsDataBuffer<Paragraph> writerBuffer = new ParagraphsDataBuffer<>(new LinkedBlockingQueue<Paragraph>());
        logger.info("Created reader and writer buffers");

        // Created ans started thread reader
        new Thread(new ParagraphsReader(oldFile, readerBuffer, syncReader)).start();
        logger.info(" Created and started thread reader");

        // Created and started threads worker
        createAndStartedThreadsWorker(amountThreadsWorker, readerBuffer, writerBuffer, syncReader, syncWriter);

        // Created and started thread writer
        Thread paragraphWriter = new Thread(new ParagraphsWriter(syncWriter, nameNewFile, writerBuffer));
        paragraphWriter.start();
        try {
            paragraphWriter.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        logger.info(" Finished thread Coordinator");

    }


    /**
     * The method created and started threads worker
     *
     * @param amountThreadsWorker  // amount threads worker
     * @param readerBuffer         // buffer for reade data which need load
     * @param writerBuffer         // buffer for write data after load
     * @param syncReader           // object for synchronization
     * @param syncWriter           // object for synchronization
     */
    public void createAndStartedThreadsWorker(int amountThreadsWorker, ParagraphBuffer<Paragraph> readerBuffer, ParagraphsDataBuffer<Paragraph> writerBuffer, Object syncReader, Object syncWriter){

        int counter = 0;
        while (counter < amountThreadsWorker){

            new Thread(new Worker(readerBuffer, writerBuffer, syncReader, syncWriter)).start();
            counter++;
            logger.info(" Created and started thread Worker " + counter + " from " + amountThreadsWorker);

        }

    }
}

package com.j2core.sts.stssecurehashfile;

import com.google.common.base.CharMatcher;
import com.google.common.base.Charsets;
import com.google.common.base.Splitter;
import com.google.common.hash.Hashing;
import com.j2core.sts.stssecurehashfile.stsbuffer.Paragraph;
import com.j2core.sts.stssecurehashfile.stsbuffer.ParagraphBuffer;
import com.j2core.sts.stssecurehashfile.stsbuffer.ParagraphsDataBuffer;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by sts on 11/19/15.
 */
/*
   The class is thread for identify data about paragraph
 */
public class Worker implements Runnable {

    public static final Logger logger = Logger.getLogger(Worker.class);
    final String SYMBOLS_PUNCTUATION = ".,:;!?()[]{}<>/|@#$%^&*-+=_~`\"";      // symbols punctuation
    final char SPACE = ' ';                                                    // symbol space
    final Object syncReader;                                                   // object for synchronization
    final Object syncWriter;                                                   // object for synchronization
    ParagraphBuffer<Paragraph> readerBuffer;                                   // buffer data for work
    ParagraphsDataBuffer<Paragraph> writerBuffer;                              // buffer for save result work
    ParagraphsDataBuffer<String> workerBuffer = new ParagraphsDataBuffer<String>(new LinkedList<String>());// buffer for job worker
    Paragraph paragraph;                                                       // object Paragraph


    /**
     * Constructor for thread Worker
     *
     * @param readerBuffer   buffer data for work
     * @param writerBuffer   buffer for save result work
     * @param syncReader     object for synchronization
     * @param syncWriter     object for synchronization
     */
    public Worker(ParagraphBuffer<Paragraph> readerBuffer, ParagraphsDataBuffer<Paragraph> writerBuffer, Object syncReader, Object syncWriter){

        this.readerBuffer = readerBuffer;
        this.writerBuffer = writerBuffer;
        this.syncReader = syncReader;
        this.syncWriter = syncWriter;
    }


    @Override
    public void run() {

        logger.info(" Started new thread Worker");

        while (!readerBuffer.isFlagEndFile() || !readerBuffer.dataQueue.isEmpty()) {

            if (readerBuffer.dataQueue.isEmpty()) {

                synchronized (syncReader){
                    syncReader.notifyAll();
                    try {
                        syncReader.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
            else {
                // take paragraph for work
                synchronized (syncReader) {
                    if (readerBuffer.dataQueue.isEmpty()){
                        break;
                    }else paragraph = readerBuffer.dataQueue.remove();
                }

                String text = paragraph.getParagraph();

                // save paragraph
                workerBuffer.setParagraphsText(text);

                // Identify paragraph's SHA1
                workerBuffer.setSha1paragraph(Hashing.sha1().hashString(text, Charsets.UTF_8).toString());

                // Calculated  amount symbols punctuation in the paragraph
                workerBuffer.setAmountSymbolPunctuation(calculationSymbolsPunctuation(text));

                // Parse paragraph
                workerBuffer.dataQueue = parserWords(text);

                // identify amount words in the paragraph
                workerBuffer.setAmountWordsInParagraph(workerBuffer.dataQueue.size());

                // Calculated length all words paragraph
                workerBuffer.setLengthAllWords(calculationLengthAllWordInParagraph(workerBuffer.dataQueue));

                // Search min word's length
                workerBuffer.setMinLengthWord(searchMinLengthWord(workerBuffer.dataQueue));

                // Search max word's length
                workerBuffer.setMaxLengthWord(searchMaxLengthWord(workerBuffer.dataQueue));

                // calculated average word's length
                workerBuffer.setAverageLengthWord(workerBuffer.getLengthAllWords() / workerBuffer.getAmountWordsInParagraph());

                // cleared queue with paragraph's words for reduce amount used resources
                workerBuffer.dataQueue.clear();

                logger.info(" Save paragraph's data in the STSWriterBuffer");
                addedParagraphsInformationInWriterBuffer();

                synchronized (syncWriter){
                    syncWriter.notify();
                }

                logger.info(" Mount buffer's parameters in the start value");
                workerBuffer = cleanParametersInWorkerBuffer(workerBuffer);
            }
        }

        lastActionsWorkerThreads(writerBuffer);
        logger.info(" Thread Worker ended work.");

    }


    /**
     * The method is parsing paragraph
     *
     * @param text       paragraph
     * @return           queue with paragraph's words
     */
    public Queue<String> parserWords( String text){

        Queue<String> buffer = new LinkedList<String>();

        Splitter iterator = Splitter.on(CharMatcher.anyOf(SPACE + SYMBOLS_PUNCTUATION));

        buffer.addAll(iterator.splitToList(text));

        while (buffer.contains("")){
            buffer.remove("");
        }

        return buffer;
    }


    /**
     * The method is calculating amount symbols punctuation
     *
     * @param text     paragraph
     * @return              amount symbols punctuation
     */
    public int calculationSymbolsPunctuation(String text){

        int amountSymbolPunctuation = 0;
        char[] data = text.toCharArray();
        for (char aData: data){

            if (SYMBOLS_PUNCTUATION.contains(String.valueOf(aData))){
                amountSymbolPunctuation++;
            }
        }
        return amountSymbolPunctuation;
    }


    /**
     * The method is searching min word's length
     *
     * @param buffer   queue with all words
     * @return         value min word's length
     */
    public int searchMinLengthWord(Queue<String> buffer){

        int minLengthWord = buffer.peek().length();
        for (String word: buffer){
            if (minLengthWord > word.length()){
                minLengthWord = word.length();
            }
        }
        return minLengthWord;
    }


    /**
     * The method is searching max length word
     *
     * @param buffer   queue with all words
     * @return         value max word's length
     */
    public int searchMaxLengthWord(Queue<String> buffer){

        int maxLengthWord = buffer.peek().length();
        for (String word: buffer){
            if (maxLengthWord < word.length()){
                maxLengthWord = word.length();
            }
        }
        return maxLengthWord;
    }


    /**
     * The method is calculation all word's length in the paragraph
     *
     * @param buffer   queue with all words
     * @return         amount symbols in the paragraph's words
     */
    public int calculationLengthAllWordInParagraph(Queue<String> buffer){

        int lengthAllWord = 0;
        for (String word: buffer){
            lengthAllWord = lengthAllWord + word.length();
        }

        return lengthAllWord;
    }


    /**
     * The method is mount all parameters in the start value
     *
     * @param workerBuffer     buffer for clean
     * @return                 buffer after clean
     */
    public ParagraphsDataBuffer<String> cleanParametersInWorkerBuffer(ParagraphsDataBuffer<String> workerBuffer){

        workerBuffer.setSha1paragraph("");
        workerBuffer.setAmountSymbolPunctuation(0);
        workerBuffer.setAmountWordsInParagraph(0);
        workerBuffer.setLengthAllWords(0);
        workerBuffer.setMinLengthWord(0);
        workerBuffer.setMaxLengthWord(1);
        workerBuffer.setAverageLengthWord(0);
        workerBuffer.setParagraphsText("");

        return workerBuffer;
    }


    /**
     * The method is adding paragraph's information in the writerBuffer
     */
    public synchronized void addedParagraphsInformationInWriterBuffer(){

        paragraph.setParagraph(System.lineSeparator() + workerBuffer.getParagraphsText() + System.lineSeparator() + " Paragraph has: "
                + System.lineSeparator() + " value SHA1: " + workerBuffer.getSha1paragraph() + ", " + workerBuffer.toString() + System.lineSeparator());

        writerBuffer.dataQueue.add(paragraph);

        writerBuffer.setAmountWordsInParagraph(writerBuffer.getAmountWordsInParagraph() + workerBuffer.getAmountWordsInParagraph());

        if (writerBuffer.getMinLengthWord() == 0){
            writerBuffer.setMinLengthWord(workerBuffer.getMinLengthWord());
        }
        else if (writerBuffer.getMinLengthWord() > workerBuffer.getMinLengthWord()){
            writerBuffer.setMinLengthWord(workerBuffer.getMinLengthWord());
        }

        if (writerBuffer.getMaxLengthWord() < workerBuffer.getMaxLengthWord()){
            writerBuffer.setMaxLengthWord(workerBuffer.getMaxLengthWord());
        }

        writerBuffer.setLengthAllWords(writerBuffer.getLengthAllWords() + workerBuffer.getLengthAllWords());

        writerBuffer.setAverageLengthWord(writerBuffer.getLengthAllWords() / writerBuffer.getAmountWordsInParagraph());

        writerBuffer.setAmountSymbolPunctuation(writerBuffer.getAmountSymbolPunctuation() + workerBuffer.getAmountSymbolPunctuation());

    }


    /**
     * The method is completing last action work thread
     *
     * @param writerBuffer       buffer for save result work
     */
    public synchronized void lastActionsWorkerThreads(ParagraphsDataBuffer<Paragraph> writerBuffer){


        if (Coordinator.amountThreadsWorker == 1){

            logger.info("  Change parameter flagAndFile in the writerBuffer.");
            writerBuffer.setFlagEndFile(true);

            synchronized (syncWriter){
                syncWriter.notify();
            }
        }

        Coordinator.amountThreadsWorker--;

    }
}

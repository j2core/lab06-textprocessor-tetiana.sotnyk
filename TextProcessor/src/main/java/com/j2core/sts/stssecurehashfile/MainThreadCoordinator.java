package com.j2core.sts.stssecurehashfile;

/**
 * Created by sts on 11/25/15.
 */

import java.io.File;

/**
 * Created and started thread coordinator
 */
public class MainThreadCoordinator {

    protected static String nameFile = "Scott_Fitzgerald.txt";              // file's path for load
    protected static String nameNewFile = "New_File_TextAnalyzer.txt";      // file's name new file with results load
    protected static int amountThreadsWorker = 8;                           // amount threads worker
    protected static int amountDataForOperation = 12;                       // max amount paragraphs in the buffer in one time

    public static void main(String[] args) {

        // created thread coordinator
        new Thread(new Coordinator(new File(nameFile).getAbsoluteFile(), nameNewFile, amountThreadsWorker, amountDataForOperation)).start();

    }
}
